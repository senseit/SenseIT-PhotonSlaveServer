
#include "Network/Router.hpp"
#include "Network/RestPaths.hpp"
#include "MDNS.h"
#include <vector>

#define HTTP_PORT 80
#define ALT_HTTP_PORT 8080

MDNS mdns;

SYSTEM_THREAD(ENABLED);
// TODO change this to semiAutomatic in case of disconnect from particle cloud
SYSTEM_MODE(AUTOMATIC);

SerialLogHandler logHandler(LOG_LEVEL_TRACE);
Router router;

// Web server port
int webServerPort = ALT_HTTP_PORT;


// Web server
#define PREFIX ""
WebServer webserver(PREFIX, webServerPort);



void initMDNS (){
  std::vector<String> subServices;

  bool success = mdns.setHostname("Photon");
  if (success) {
    success = mdns.addService("tcp", "customhttp", HTTP_PORT, "Photon", subServices);
  }

  mdns.addTXTEntry("normal");

  if (success) {
    success = mdns.addService("tcp", "http", ALT_HTTP_PORT, "Photon Service");
  }

  mdns.addTXTEntry("DeviceID",System.deviceID());

  IPAddress myIp = WiFi.localIP();
  char myIpAddress[24];
  sprintf(myIpAddress, "%d.%d.%d.%d", myIp[0], myIp[1], myIp[2], myIp[3]);

  mdns.addTXTEntry("IP",myIp);

  char port[6];
  sprintf(port, "%d", webServerPort);
  mdns.addTXTEntry("Port", port);

  if (success) {
    // Announce on startup
    success = mdns.begin(true);
  }
}

void setup()
{
  Serial.begin(115200);
  delay(3000);
  Log.info("Starting SenseIT particle server");

  while(1)
  {
      if (WiFi.ready())
        break;
      delay(5000);
      Log.warn("Waiting for WiFi");
  }

  initMDNS();

  IPAddress myIp = WiFi.localIP();
  char myIpAddress[24];
  sprintf(myIpAddress, "%d.%d.%d.%d", myIp[0], myIp[1], myIp[2], myIp[3]);
  Serial.printlnf("IP Address %s%d", myIpAddress, webServerPort);

  webserver.setDefaultCommand(router.restAPI_IsOnline);

  webserver.addCommand(RestPaths::IsOnline, router.restAPI_IsOnline);
  webserver.addCommand(RestPaths::SetTriggerSensor, router.restAPI_AddNewRule);
  webserver.addCommand(RestPaths::DeleteTriggerSensor, router.restAPI_DeleteNewRule);
  webserver.begin();
}

void loop()
{
  mdns.begin(true);
  mdns.processQueries();
  // Service the web server
  webserver.processConnection();
  router.service();
}
