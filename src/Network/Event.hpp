#pragma once

#include <string>

class Event{
public:
  std::string message;
  std::string input;
};
