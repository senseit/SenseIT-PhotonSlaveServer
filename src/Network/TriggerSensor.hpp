#include "EventStorage.hpp"
#include <sstream>

class TriggerSensor{
private:
  int triggerPort;
  int outputPort;
  uint8_t triggerValue;
public:
  TriggerSensor(int triggerPort, int outputPort, uint8_t triggerValue):
    triggerPort(triggerPort),
    outputPort(outputPort),
    triggerValue(triggerValue)
  {
    pinMode(triggerPort, INPUT);
    pinMode(outputPort, OUTPUT);
  }

  int   getTriggerPort(){ return triggerPort; }
  void  setTriggerPort(int triggerPort) { this->triggerPort = triggerPort; }

  int   getOutputPort(){ return outputPort; }
  void  setOutputPort(int outputPort) { this->outputPort = outputPort; }

  int   getTriggerValue(){ return triggerValue; }
  void  setTriggerValue(int triggerValue) { this->triggerValue = triggerValue; }

  void  service(){
    int buttonState = digitalRead(triggerPort);
    if (buttonState == triggerValue) {
       digitalWrite(outputPort, HIGH);
       if (!EventStorage::getInstance().containsEvent("Trigger event", triggerPort)){
         EventStorage::getInstance().addEvent("Trigger event", triggerPort);
         Serial.printlnf("Event triggered");
       }
    } else {
       digitalWrite(outputPort, LOW);
    }
  }
};
