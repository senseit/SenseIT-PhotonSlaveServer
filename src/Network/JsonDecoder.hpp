enum PortType{
  TriggereSensor = 0,
  MeasurementSensor = 1
};

struct TriggerDesc{
  PortType  type;
  int       inputPort;
  int       outputPort;
};

class JsonDecoder{
public:
  static TriggerDesc convertString (const char* sensorType, const char* sensorPort, const char* outputSensorPort){
    TriggerDesc triggerDesc;
    if (strcmp(sensorType, "Trigger Sensor") == 0){
      triggerDesc.type = TriggereSensor;
    }

    if (strcmp(sensorType, "Measuring Sensor") == 0){
      triggerDesc.type = MeasurementSensor;
    }

    std::vector<int> photonDigitalPortList = {D0,D1,D2,D3,D4,D5,D6,D7};
    std::vector<String> restDigitalPortList = { "Digital 0",
                                                "Digital 1",
                                                "Digital 2",
                                                "Digital 3",
                                                "Digital 4",
                                                "Digital 5",
                                                "Digital 6",
                                                "Digital 7" };

    int i=0;
    for (const auto& restPort: restDigitalPortList){
      if (strcmp(sensorPort, restPort) == 0){
        triggerDesc.inputPort = photonDigitalPortList[i];
        Serial.printf("talalt portot i = %d, port = %d\n", i, photonDigitalPortList[i]);
        break;
      }
      i++;
    }

    i=0;
    for (const auto& restPort: restDigitalPortList){
      if (strcmp(outputSensorPort, restPort) == 0){
        triggerDesc.outputPort = photonDigitalPortList[i];
        Serial.printf("talalt portot i = %d, port = %d\n", i, photonDigitalPortList[i]);
        break;
      }
      i++;
    }
    return triggerDesc;
  }
};
