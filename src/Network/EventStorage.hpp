#pragma once

#include "Event.hpp"
#include <vector>
#include <string>
#include <sstream>

class EventStorage{
private:
   std::vector<Event> events;
public:
  EventStorage(){}

  static EventStorage& getInstance(){
    static EventStorage storage;
    return storage;
  }

  bool containsEvent(char message[], int port){
    for (const auto& event: events){
      char str[10];
      sprintf(str, "%d", port);
      if (event.message == message && event.input == str){
        return true;
      }
    }
    return false;
  }

  void addEvent(char message[], int port) {
    Event event;
    event.message = message;
    char str[10];
    sprintf(str, "%d", port);
    event.input = str;
    events.push_back(event);
  }

  void clearEvents(){
    events.clear();
  }

  std::vector<Event> getEvents(){
    return events;
  }

};
