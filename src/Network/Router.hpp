#include "TriggerSensor.hpp"
#include <vector>
#include "WebDuino.h"
#include <ArduinoJson.h>
#include "JsonDecoder.hpp"
#include "EventStorage.hpp"

class Router{
public:
  static std::vector<TriggerSensor> sensorList;

  Router(){}

  void service(){
    for (auto& triggerSensor: sensorList){
      triggerSensor.service();
    }
  }

  static void restAPI_AddNewRule(WebServer& server, WebServer::ConnectionType type, char *, bool)
  {
    if (type == WebServer::POST)
    {
      bool repeat;
      char name[300], value[300];

      repeat = server.readPOSTparam(name, 300, value, 300);

      StaticJsonBuffer<400> jsonBuffer;
      JsonObject& root = jsonBuffer.parseObject(name);

      const char* sensorPort = root["sensorPort"];
      Serial.printf("Input Sensor port - %s\n", sensorPort);

      const char* sensorType = root["sensorType"];
      Serial.printf("Input sensor Type - %s\n", sensorType);

      const char* outputSensorPort = root["outputSensorPort"];
      Serial.printf("output sensor port - %s\n", outputSensorPort);

      TriggerDesc triggerDesc = JsonDecoder::convertString(sensorType, sensorPort, outputSensorPort);

      Serial.printf("Input sensor port - %d\n", triggerDesc.inputPort);
      Serial.printf("output sensor port - %d\n", triggerDesc.outputPort);

      sensorList.push_back ({triggerDesc.inputPort, triggerDesc.outputPort, HIGH});

      server.httpSuccess();
      return;
    }
  }


  static void restAPI_DeleteNewRule(WebServer& server, WebServer::ConnectionType type, char *, bool)
  {
    if (type == WebServer::POST)
    {
      bool repeat;
      char name[300], value[300];

      repeat = server.readPOSTparam(name, 300, value, 300);

      StaticJsonBuffer<400> jsonBuffer;
      JsonObject& root = jsonBuffer.parseObject(name);

      const char* id = root["deleteId"];
      Serial.printf("Delete Sensor - %s\n", id);

      int index = atoi(id);
      Serial.printf("deleteIndex %d\n", index);
      sensorList.erase(sensorList.begin() + index);
      server.httpSuccess();
      return;
    }
  }


  static void restAPI_IsOnline(WebServer &server, WebServer::ConnectionType type, char *, bool)
  {
    if (type == WebServer::GET)
    {

      StaticJsonBuffer<400> jsonBuffer;
      JsonArray& array = jsonBuffer.createArray();

      for (const auto& event: EventStorage::getInstance().getEvents()){
        JsonObject& nested = array.createNestedObject();
        nested["message"] = event.message.c_str();
        nested["input"] = event.input.c_str();
      }

      char jsonChar[500];
      array.printTo((char*)jsonChar, array.measureLength() + 1);

      server.httpSuccess();
      server.printP(jsonChar);
      Serial.printf(jsonChar);
      EventStorage::getInstance().clearEvents();
      return;
    }
  }

};

std::vector<TriggerSensor> Router::sensorList;
