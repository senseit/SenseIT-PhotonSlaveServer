# SenseIT - Photon Slave Server

The central functionality of the C++ Photon slave server is the sensor rule and event management. The slave server observes and collects environmental changes, which are requested and processed by the central master server. 

## Getting started
Instructions for deploy the slave server to the Particle Photon Device.

### Particle Build Desktop IDE
It can be downloaded from the Particle documentation site:
https://docs.particle.io/tutorials/developer-tools/dev/

The project can be loaded by folder, and flash to the Photon with the aid of the desktop IDE.

### Particle CLI
It can be downloaded from the Particle documentation site:
https://docs.particle.io/tutorials/developer-tools/cli/

After a successful login to the cloud, the **BuildScript.bat**  flashes the firmware to the Photon.

## License

This project is licensed under the GPL License - see the [LICENSE.md](https://gitlab.com/senseit/SenseIT-PhotonSlaveServer/blob/master/LICENSE.md)  file for details


